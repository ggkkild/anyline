<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/wap/common/inc/tag.jsp"%>
<div class="btn-group" style="float:right;width:100%;">
	<div style="float:right;">
		<form action="" id="frm">
			<al:select id="cboSort" data="${sorts }" name="sort" value="${param.sort }" head="类别"></al:select>
			<div class="btn" onclick="$('#frm').submit();" style="width:100px;">查询</div>
			<a class="btn" href="a" style="width:100px;">添加</a>
		</form>
	</div>
</div>
<table class="list" style="width:100%;">
	<tr class="list_head">
		<td>序号</td>
		<td>名称</td>
		<td>类别</td>
		<td>价格</td>
		<td>明细</td>
	</tr>
	<c:forEach var="item" items="${set }" varStatus="status">
		<tr id="tr_book_${item.ID }">
			<td>${status.index + 1 + navi.pageRows * (navi.curPage-1)}</td>
			<td>${item.TITLE }</td>
			<td>${item.SORT_NM }</td>
			<td>${item.PRICE}</td>
			<td><a href="v?id=${item.ID }">查看</a> | <a href="u?id=${item.ID }">修改</a></td>
		</tr>
	</c:forEach>

</table>
${navi}