function al(){}
al.url = null;					//请求的URL
al.value = 'CD';				//键值对时 KEY
al.label = 'NM';				//键值对时 VALUE
al.async = true;				//是否异步请求
al.data = null;					//请求参数
al.user = null;					//用户名
al.password = null;				//密码
al.success = function(o){};		//请求成功时的回调函数 与callback相同
al.callback = function(o){};	//请求成功时的回调函数 与success相同
al.fail = function(data,msg){};	//请求失败的回调函数
al.type = 'POST';				//请求方法类型
al.form = null;					//提交的form
al.xhr = null;					//xhr
al.dataType = 'json';			//返回的数据类型
al.json = null;					//返回的数据
var __is_ajax_run = false;
function init(config){
	if(typeof config['async'] == "undefined") {config['async'] = true;}
	if(!config['success']){config['success']=function(data){};}
	if(!config['callback']){config['callback']=function(result,data,msg){};}
	if(!config['fail']){config['fail']=function(data,msg){};}
	config['_anyline_request_time'] = new Date().getMilliseconds();
	return config;
}
al.submit = function(frm,config){
	if(!frm){return false;}
	//if(__is_ajax_run){return false;}
	config = config.init(config);
	__is_ajax_run = true;
	$(frm).ajaxSubmit({
		type:'post',
		dataType: 'json',
		success:function(json){
			__is_ajax_run = false;
			var result = json['result'];
			var message = json['message'];
			var url = json['url'];
			if(url){
				location.href = url;
			}
			var data;
			var type = json['type'];
			config['json'] = json;
			_ajax_success(config);
		},
	   error:function(XMLHttpRequest, textStatus, errorThrown) {
			__is_ajax_run = false;
	   		_ajax_error(XMLHttpRequest, textStatus, errorThrown);
	   }
	});
	
};
al.ajax = function(config){
	if(__is_ajax_run){
		//return false;
	}
	config = init(config);
	try{
		if(arguments && arguments.callee && arguments.callee.caller){
			config.data['caller'] = arguments.callee.caller.name;
		}
	}catch(e){}
	__is_ajax_run = true;
	$.ajax({
	   async: config.async,
	   type: 'post',
	   url: config.url,
	   data: config.data,
	   dataType: 'json',
	   success: function(json){
		   __is_ajax_run = false;
			var url = json['url'];
			if(url){
				location.href = url;
				return;
			}
	   		config.json = json;
			_ajax_success(config);
	   },
	   error:function(XMLHttpRequest, textStatus, errorThrown) {
		   __is_ajax_run = false;
	   	   _ajax_error(XMLHttpRequest, textStatus, errorThrown);
	   }
	});
};

function _ajax_success(config){
	var result = config.json['result'];
	var message = config.json['message'];
	var data;
	var type = config.json['type'];
	
	//解析数据
	if(type=='string' || type=='number'){
		data = config.json['data'];
	}else if(type=='map'){
		data = config.json['data'];
	}else if(type == 'list'){
		var tmp = config.json['data'];
		data = tmp;
	}
	//附加操作方法
	if(data){
		data.get=function(idx,key){
			if(!key){
				key = idx;
				idx = 0;
			}
			if(data.length && idx<data.length){
				return data[idx][key];
			}else{
				return null;
			}
		};
	}
	if(result){
		//函数回调
		var success = config.success;
		if(success){
			success(data,message);
		}
	}else{
		var fail = config.fail;
		if(fail){
			fail(data,message);
		}
		
	}

	var code = config.json['code'];
	var redirect = config.json['redirect'];
	if(redirect){
		location.href= redirect;
	}
	var jscall = config.json['jscall'];
	if(jscall){
		eval(jscall);
	}


	var callback = config.callback;
	if(callback){
		callback(result,data,message,config);
	}
};
function _ajax_error(XMLHttpRequest, textStatus, errorThrown){
	//config.lock();
//	if(typeof(art) != "undefined"){
//		art.dialog({content:XMLHttpRequest.responseText});
//	}else{
		console.log("状态:"+textStatus+"\n消息:"+XMLHttpRequest.responseText);
//	}
};

/**
 * 加载服务器端文件
 * path必须以密文提交 <al:des>/WEB-INF/template/a.jsp</al:des>
 * 以WEB-INF为相对目录根目录
 * al.template('/WEB-INF/template/a.jsp',function(result,data,msg){alert(data)});
 * al.template({path:'template/a.jsp', id:'1'},function(result,data,msg){});
 * 模板文件中以${param.id}的形式接收参数
 * 
 * 对于复杂模板(如解析前需要查询数据)需要自行实现解析方法js中 通过指定解析器{parser:'/al/tmp/load1.do'}形式实现
 *controller中通过 WebUtil.parseJsp(request, response, file)解析JSP
 *注意 parsejsp后需要对html编码(以避免双引号等字符在json中被转码) js接收到数据后解码
 *escape unescape
 */
var _anyline_template_file= {};
al.template = function(config, fn){
	if(typeof config == 'string'){
		config = {path:config};
	}
	var parser_url = '/al/tmp/load';
	if(config['parser']){
		parser_url = config['parser'];
	}
	var cache = true;
	if(config['cache'] == false){
		cache = false;
	}
	var key = parser_url + "_" + config['path'];
	if(cache && _anyline_template_file[key]){
		fn(true,_anyline_template_file[key],'');
		return;
	}
	al.ajax({
		url:parser_url,
		data:config,
		callback:function(result,data,msg){
			data = unescape(data);
			_anyline_template_file[key] = data;
			fn(result,data,msg);
		}
	});
}

//验证form p:form msgBox:异常信息显示位置
al.validate = function(p, msgBox){
	var result = true;
	$(p).find('.required').each(function(item){
		var val = $(this).val();
		if(!val){
			var title = $(this).attr('placeholder');
			$(this).focus();
			if(!title){
				title = '请确认信息完整';
			}else{
				if(title.indexOf('输入') != -1 || title.indexOf('确认') != -1){
				}else{
					title = '请确认'+title;
				}
			}
			al.tips(title);
			$(msgBox).html(title);
			result = false;
			return false;
		}
	});
	return result;
  }


//封装form参数
al.packParam = function(p, src){
	var result = src;
	if(!result){
		result = {};
	}
	$(p).find('input').each(function(item){
		var val = $(this).val();
		var key = $(this).attr('name');
		if(key){
			if($(this).attr('type') == 'checkbox'){
				if($(this).is(':checked')){
					result[key] = fnPushParam(result[key],val);
				}
			}else if($(this).attr('radio') == 'checkbox'){
				if($(this).is(':checked')){
					result[key] = fnPushParam(result[key],val);
				}
			}else{
				result[key] = fnPushParam(result[key],val);
			}
		}
	});

	$(p).find('textarea').each(function(item){
		var val = $(this).val();
		var key = $(this).attr('name');
		if(key){
			result[key] = fnPushParam(result[key],val);
		}
	});

	$(p).find('select').each(function(item){
		var val = $(this).val();
		var key = $(this).attr('name');
		if(key){
			result[key] = fnPushParam(result[key],val);
		}
	});

	return result;
}
function fnPushParam(old, val){
	if(null != old){
		if(old instanceof Array){
			old.push(val);
			return old;
		}else{
			var list = new Array();
			list.push(old);
			list.push(val);
			return list;
		}
	}else{
		return val;
	}
}


al.initSelect = function(config){
	var select = null;
	var data = null;
	var valueKey = 'ID';
	var textKey = 'NM';
	var headerValue = '';
	var headerText = null;
	var defaultValue = null;
	var clear = true;
	if(typeof config['select'] != "undefined") {
		select = config['select'];
	}
	if(typeof config['data'] != "undefined") {
		data = config['data'];
	}
	if(typeof config['valueKey'] != "undefined") {
		valueKey = config['valueKey'];
	}
	if(typeof config['textKey'] != "undefined") {
		textKey = config['textKey'];
	}
	if(typeof config['defaultValue'] != "undefined") {
		defaultValue = config['defaultValue'];
	}
	if(typeof config['headerText'] != "undefined") {
		headerText = config['headerText'];
	}
	if(typeof config['defaultValue'] != "undefined") {
		defaultValue = config['defaultValue'];
	}
	if(typeof config['clear'] != "undefined") {
		clear = config['clear'];
	}
	if(!select){
		return;
	}
	if(clear){
		$(select).empty();
	}
	if(headerText != null ){
		$(select).append('<option value="'+headerValue+'">' + headerText + '</option>');
	}
	if(data){
		var size = data.length;
		for(var i=0; i<size; i++){
			var value = data[i][valueKey];
			var text = data[i][textKey];
			var item = '<option value="'+value+'"';
			if(value == defaultValue){
				item += ' selected';
			}
			item += '>' + text + '</option>';
			$(select).append(item);
		}
	}
}

////////////////////////////////////////////// TIPs


/** 
 * 使用方式：
 *      tips("xx","bottom",1000);
 *      tips("xxx","bottom");
 *      tips("xxx",1000);
 *      tips("xxx");
 */
al.tips = function() {
	
  //默认设置
  var dcfg={
      msg:"提示信息",
      postion:"middle",//展示位置，可能值：bottom,top,middle,默认middle：是因为在mobile web环境下，输入法在底部会遮挡toast提示框
      time:3000,//展示时间
    };
  //*********************以下为参数处理******************
  var len = arguments.length;
  var arg0 =arguments[0];
  if(arguments.length>0){
    dcfg.msg =arguments[0];
    dcfg.msg=arg0;
    console.log('tips msg:'+arg0);
    var arg1 =arguments[1];
    var regx = /(bottom|top|middle)/i;
    var numRegx = /[1-9]\d*/;
    if(regx.test(arg1)){
      dcfg.postion=arg1;
    }else if(numRegx.test(arg1)){
      dcfg.time=arg1;
    }
     
    var arg2 =arguments[2];
    var numRegx = /[1-9]\d*/;
    if(numRegx.test(arg2)){
      dcfg.time=arg2;
    }
  }
//*********************以上为参数处理******************
var ret = "<div class='web_toast'><div class='cx_mask_transparent'></div>" + dcfg.msg + "</div>";
$(".web_toast").remove();
 
    $("body").append(ret);
 
  var w = $(".web_toast").width(),ww = $(window).width();
  $(".web_toast").show('slow');
  $(".web_toast").css("left",(ww-w)/2-20);
  if("bottom"==dcfg.postion){
    $(".web_toast").css("bottom",50);
    $(".web_toast").css("top","");
  }else if("top"==dcfg.postion){
    $(".web_toast").css("bottom","");
    $(".web_toast").css("top",50);
  }else if("middle"==dcfg.postion){
	$(".web_toast").css("bottom","");
	$(".web_toast").css("top","");
    var h = $(".web_toast").height(),hh = $(window).height();
    $(".web_toast").css("bottom",(hh-h)/2-20);
  }
  setTimeout(function() {	
    $(".web_toast").remove();//.hide('slow');	
  }, dcfg.time);
}


al.getFnName = function(callee){
    var _callee = callee.toString().replace(/[\s\?]*/g,""),
    comb = _callee.length >= 50 ? 50 :_callee.length;
    _callee = _callee.substring(0,comb);
    var name = _callee.match(/^function([^\(]+?)\(/);
    if(name && name[1]){
      return name[1];
    }
}
al.isWeixin = function(){  
    var u = navigator.userAgent.toLowerCase();  
    if(u.match(/MicroMessenger/i)=="micromessenger") {  
        return true;  
    } else {  
        return false;  
    }  
} 
al.isAndroid = function(){
	var u = navigator.userAgent.toLowerCase();
	var result = u.indexOf('android') > -1 || u.indexOf('adr') > -1; //android终端
	return result;
}
al.isIOS = function(){
	var u = navigator.userAgent.toLowerCase()
	var result = !!u.match(/\(i[^;]+;( u;)? cpu.+mac os x/); //ios终端
	return result;
}
