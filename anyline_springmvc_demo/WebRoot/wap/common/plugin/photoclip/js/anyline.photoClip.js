
al.imgclip=al.photoclip = function(conf){
	var width = conf['width'];
	var height = conf['height'];
	var fnCallback = conf['callback'];
	var url = conf['url'];
	var flag = conf['flag'];
	if(!flag){
		flag = Math.ceil(Math.random()*1000);
	}
	var fileId = "clip_file" + flag;
	var btnId = "clip_btn" + flag;
	var labId = "clip_label" + flag;
	var style = "margin: 10px 0px 0px 0px;display:inline-block;text-align:center;vertical-align:center;max-width:200px;line-height:35px;";
	var html = '<label for="' + fileId + '"><div id="clip_area'+flag+'" style="height: '+(height+50)+'px;"></div></label>'
		+ '<div style="text-align:center;"><input type="file" id="' + fileId + '" style="display:none;" >'
		+ '<div><label id="' + labId + '" for="' + fileId + '" class="clip_btn btn" style="'+style+'">选择图片</label></div>'
		+ '<div><label id="' + btnId + '" class="clip_btn btn" style="'+style+'">上传图片</label></div>'
		+ '<div id="clip_view'+flag+'"></div></div>';
	layer.open({
		  content: html,
		  btn: '取消',
		  shadeClose: false,
		  yes: function(index){
			  layer.close(index);
		  }
		});

	$("#clip_area"+flag).photoClip({
		width: width,
		height: height,
		file: "#clip_file"+flag,
		view: "#clip_view"+flag,
		ok: "#clip_btn"+flag,
		loadStart: function() {
			$('#'+labId).text('图片读取中...');
			console.log("图片读取中");
		},
		loadComplete: function() {
			$('#'+labId).text('选择图片');
			console.log("图片读取完成");
			$('#'+btnId).text('上传图片');
		},
		clipFinish: function(dataURL) {
			if($('#'+btnId).text() != '上传图片'){
				console.log("图片正在上传，请稍后操作");
				return;
			}
			$('#'+btnId).text('图片上传中...');
			al.ajax({
				url:url,
				data:{str:dataURL},
				callback:function(result,data,msg){
					$('#'+btnId).text('上传图片');
					fnCallback(result,data,msg, conf);
				}
			});
		}
	});
}